#!/bin/bash
. ./env
UNIT=demoavro
rm -rf $UNIT
mkdir -p $UNIT
mkdir -p $UNIT/data
cp product/*.jar src/queue/jars/*.jar src/avro/jars/*.jar src/publish/jars/*.jar src/subscribe/jars/*.jar $UNIT/.
cat <<'!eof' >$UNIT/log4j.properties
log4j.debug=FALSE
log4j.rootLogger=INFO,Root

log4j.appender.Root=org.apache.log4j.DailyRollingFileAppender
log4j.appender.Root.file=subscriber.log
log4j.appender.Root.datePattern='.'yyyyMMdd
log4j.appender.Root.append=true
log4j.appender.Root.layout=org.apache.log4j.PatternLayout
log4j.appender.Root.layout.ConversionPattern=%d %p %t %m%n
!eof
#
# this avro schema is specific to the sample data, which is the output of ps -ef
cat <<!eof >$UNIT/$UNIT.avsc
{"namespace": "linux.avro",
 "type": "record",
 "name": "ps",
 "fields": [
     {"name": "uid", "type": ["int", "string"]},
     {"name": "pid", "type": ["int", "string"] },
     {"name": "ppid", "type": ["int", "string"] },
     {"name": "c", "type": ["int", "string"] },
     {"name": "stime", "type": "string"},
     {"name": "tty", "type": "string"},
     {"name": "time", "type": "string"},
     {"name": "cmd", "type": "string"}
 ]
}
!eof
cat <<!eof >$UNIT/configfile.properties
#QueueDirectories: queues
Processors: serial, deserial, received
serial.Class: com.att.datarouter.pubsub.avro.SerializeFile
serial.AvroHeader: $UNIT.avsc
serial.Destination: deserial
#serial.Delimiter: [ ]*
deserial.Class: com.att.datarouter.pubsub.avro.DeserializeFile
deserial.Destination: received
deserial.Delimiter: \t
received.Class: com.att.datarouter.pubsub.queue.TrivialProcessor
#received.Threads: 1
Factories: scanner
scanner.Class: com.att.datarouter.pubsub.filescan.FileScanner
scanner.Destination: serial
#scanner.Directory: 
#scanner.MinAgeSeconds: 10
#scanner.ScanIntervalSeconds: 10
scanner.DeleteOrGZip: gzip
#scanner.GZipRetentionDays: 30
!eof
cd $UNIT
# create the file to be read
ps -ef > data/sample_data
# start up the pubsub queue
CLASSPATH=$(echo . *.jar | tr ' ' ':') java $TRUSTARG com.att.datarouter.pubsub.queue.Main
