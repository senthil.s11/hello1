::Setup config file with following paramaters. 
::instructions to run the batch file (The arguments are optional)
::demoDosBatchPublisher.bat [arg1 arg2 arg3] (e.g demoDosBatchPublisher.bat c:/test test.txt log)

::FILEPATH=mypath/tmp
::LOGFILE_PATH=publisher.log
::FILENAME=test.txt (it should be the file which is going to be publised to Data Router)
::PUBLISHER_USER=user (should be correct username of publisher.)
::PUBLISHER_PASSWORD=pass (should be correct password of publisher)
::PUBLISH_URL=https://feeds-uat-drtr.web.att.com/publish/369 (should be changed for each feed)
::LOGTYPE=log (should be either log or return or debug)

::----------Example--------------
::FILEPATH=./
::LOGFILE_PATH=publisher.log
::FILENAME=test.zip
::PUBLISHER_USER=user
::PUBLISHER_PASSWORD=pass
::PUBLISH_URL=https://feeds-uat-drtr.web.att.com/publish/369
::LOGTYPE=log
::-------End Example------


echo off

::#Property file to configure publisher parameters, it can be modified and place to different folder.
set PROPERTYFILE=publisher.properties

if exist %PROPERTYFILE% (
    echo " %PROPERTYFILE% file exists"
) else (
    echo " %PROPERTYFILE% file doesn't exist"
)

For /F "tokens=1* delims==" %%A IN (%PROPERTYFILE%) DO (
    IF "%%A"=="FILEPATH" set FILEPATH=%%B
    IF "%%A"=="LOGFILE_PATH" set LOGFILE_PATH=%%B
    IF "%%A"=="FILENAME" set FILENAME=%%B
    IF "%%A"=="PUBLISHER_USER" set PUBLISHER_USER=%%B
    IF "%%A"=="PUBLISHER_PASSWORD" set PUBLISHER_PASSWORD=%%B
    IF "%%A"=="PUBLISH_URL" set PUBLISH_URL=%%B
    IF "%%A"=="LOGTYPE" set LOGTYPE=%%B


)



echo "%FILEPATH%"
echo "%LOGFILE_PATH%"
echo "%FILENAME%"
echo "%PUBLISHER_USER%"
echo "%PUBLISHER_PASSWORD%"
echo "%PUBLISH_URL%"
echo "%LOGTYPE%"


::Check if file path, file name and LOGTYPE is passed as argument to script.
 IF [%1]==[] (
  echo " Files Path = %FILEPATH% is taken from %PROPERTYFILE% file "
  )
 IF NOT [%1]==[] (
  set FILEPATH=%1
  echo " First Commandline Argument Files Path = %1 "
 )

 IF [%2]==[] (
  echo "File Name = %FILENAME% is taken from %PROPERTYFILE% file "
 )
 IF NOT [%2]==[] (
  set FILENAME=%2
  echo " Second Commandline Argument File Name = %2 "
 )

 IF [%3]==[] (
  echo "LOGTYPE = %LOGTYPE% is taken from %PROPERTYFILE% file "
 )
 IF NOT [%3]==[] (
  set LOGTYPE=%3
  echo " Third Commandline Argument LOGTYPE = %3 "
 )

::Log the messages
  set LOGFILE=%DATE:~-4%-%DATE:~4,2%-%DATE:~7,2%_%LOGFILE_PATH% 
 
if not exist %FILEPATH%\%FILENAME% (
   IF %LOGTYPE%==return (
    	EXIT /B 1	
   ) ELSE	 (
       :: echo " %FILEPATH%\%FILENAME% file doesn't exists. It is NOT published." >> %LOGFILE% 
        echo " %FILEPATH%\%FILENAME% file doesn't exists. It is NOT published." 
        EXIT /B 1
    )
)

::Publis file by batch script.
  for /f %%O in ('curl  -o NUL -s -v  -w "%%{http_code}" -X PUT --user %PUBLISHER_USER%:%PUBLISHER_PASSWORD% -H "Content-Type: application/octet-stream" --data-binary @%FILEPATH%/%FILENAME% --TLSv1.2 --post301 --location-trusted %PUBLISH_URL%/%FILENAME% 2^> temp.txt') do ( set "status_code=%%O"   ) 

::Error handling 
  echo Response code = %status_code%
 
::Log the messages
  set LOGFILE=%DATE:~-4%-%DATE:~4,2%-%DATE:~7,2%_%LOGFILE_PATH% 
 
 IF %LOGTYPE%==debug (
   type temp.txt >> %LOGFILE%
 )
 
  IF %status_code%==204 (
      IF %LOGTYPE%==return (
	EXIT /B 0
       ) ELSE (
	Echo File %FILENAME% successfully published. >> %LOGFILE% 
       )
  ) ELSE (
       IF %LOGTYPE%==return (
     	EXIT /B 1	
        ) ELSE	 (
         Echo Error Code = %status_code%. File %FILENAME% is NOT published. >> %LOGFILE%
 	)
  ) 


